import { Paper } from "@material-ui/core";
import { FunctionComponent } from "react";
import styled from "styled-components";
// @ts-ignore
import WidgetExport from "../assets/DateWeatherVaccinations.kwgt";
// @ts-ignore
import HomeScreen from "../assets/HomeScreen.jpg";

const Container = styled.div`
  margin-bottom: 2.3rem;
  overflow-y: scroll;
`;

const HalfImage = styled.img`
  height: 50vh;
  margin-top: 1rem;
`;

const BrightLink = styled.a`
  color: cyan;
`;

export const Widget: FunctionComponent = () => (
  <Container>
    <Paper elevation={3}>
      <p>
        Want the vaccination progress for a country on your homescreen? Download{" "}
        <BrightLink href="https://play.google.com/store/apps/details?id=org.kustom.widget">
          KWGT widget maker
        </BrightLink>{" "}
        and use this function to fetch the vaccination progress percentage for
        your country. Replace <code>:be:</code> with your country code.
      </p>
      <p>
        <code>
          $tc(split, tc(split,
          wg("https://static.dwcdn.net/data/W1FKr.csv?v=1610704740000", txt),
          ":be:,", 1), ",")$
        </code>
      </p>
    </Paper>
    <a href={HomeScreen}>
      <HalfImage src={HomeScreen} alt="Homescreen with example widget" />
    </a>
    <p>
      Or import this example from{" "}
      <BrightLink href={WidgetExport}>this file</BrightLink>
    </p>
  </Container>
);
